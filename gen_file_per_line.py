#!/usr/bin/python3

# Python Standard Library
import argparse, hashlib
from pathlib import Path
from typing import Any


def main(cmd_line_args: Any = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", type=Path)
    parser.add_argument("destdir", type=Path)
    parser.add_argument("--ext", default=".txt", help="file extension")
    args = parser.parse_args(cmd_line_args)

    assert args.ext[0] == "." 
    assert args.destdir.is_dir()

    with open(args.infile, 'rb') as file:
        for line in file:
            if line != b"\n":
                h = hashlib.md5(line).hexdigest()
                found = list(args.destdir.glob(f"{h}.*"))
                if found:
                    print(*found)
                else:
                    dest = args.destdir / f"{h}{args.ext}"
                    with open(dest, 'wb') as out:
                        out.write(line)
                    print(dest)
    return 0


if __name__ == "__main__":
    exit(main())
