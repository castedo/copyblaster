#!/bin/bash
set -o errexit

sed -i 's/\. /.\n/g' "$@"
