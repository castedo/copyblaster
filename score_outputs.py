#!/usr/bin/python3

from blast_cases import get_case_paths

import pandas as pd  # type: ignore

# Python Standard Library
import argparse, os, filecmp, math, re
from pathlib import Path
from typing import Any
from warnings import warn
from dataclasses import dataclass


def get_inputs(case_paths: list[Path], listfile: Path) -> set[Path]:
    ret = set()
    with open(listfile) as file:
        for line in file:
            line = line.strip()
            if line:
                found: set[Path] = set()
                for case in case_paths:
                    indir = case / "input"
                    found.update(p.relative_to(indir) for p in indir.glob(line))
                if not found:
                    warn("NOT FOUND: {}/{}".format(indir, line))
                ret.update(found)
    return ret


def file_lines_set(p: Path) -> set[str]:
    ret = set()
    if p.exists():
        with open(p) as file:
            a = [l for l in file.readlines() if l != "\n"]
            ret = set(a)
            if len(ret) != len(a):
                warn(f"Approx stats since file has duplicate lines: {p}")
    return ret


def dir_lines_set(dirp: Path, filesubpath: Path) -> set[str]:
    ret = set()
    if dirp.exists():
        for sub in dirp.iterdir():
            ret |= file_lines_set(sub / filesubpath)
    return ret


class CheckLines:
    def __init__(self, checkdir: Path, relp: Path):
        self.good = dir_lines_set(checkdir, "good" / relp)
        self.bad = dir_lines_set(checkdir, "bad" / relp)
        self.fail = dir_lines_set(checkdir, Path("fail"))
        failex = dir_lines_set(checkdir, Path("failex"))
        self._failex = [re.compile(s.rstrip('\n')) for s in failex]
        assert self.good.isdisjoint(self.bad)
        assert not self.found_fail_lines(self.good)

    def found_fail_lines(self, outs):
        if not outs.isdisjoint(self.fail):
            return True
        for regex in self._failex:
            if any(regex.search(line) for line in outs):
                return True
        return False


class Metrics:
    def __init__(self, ins: set[str], outs: set[str], lines: CheckLines):
        assert len(ins)

        if lines.found_fail_lines(outs):
            self.fail = -1.0
            self.good = 0.0
            self.bad = 0.0
        else:
            self.fail = 0.0
            unchanged = ins & outs
            denom = len(outs) - len(unchanged)
            if lines.bad:
                denom += len(lines.bad & unchanged)
            if lines.good:
                denom += len(lines.good & unchanged)
            self.good = len(outs & lines.good) / denom if denom else math.nan
            self.bad = -len(outs & lines.bad) / denom if denom else math.nan

        self.score = self.fail + self.good + self.bad


def score_metrics(inlines: set[str], outlines: set[str], checkp: Path, relp: Path):
    checklines = CheckLines(checkp, relp)
    if len(outlines) == 1:
        bypass = file_lines_set(checkp / "bypass")
        if outlines.issubset(bypass):
            outlines = inlines
        elif len(inlines) > 1 and outlines.isdisjoint(checklines.good):
            # output is probably something unexpected like "No changes needed."
            # so handle the output line as a fail line
            checklines.fail = outlines
    return Metrics(inlines, outlines, checklines)


@dataclass
class MetricScope:
    case: str
    view: str
    group: str
    file: str


class DFBuilder:
    def __init__(self):
        self.rows = list()

    def to_data_frame(self) -> pd.DataFrame:
        colums = ["case", "view", "test", "metric", "group", "filename", "num"]
        return pd.DataFrame(self.rows, columns=colums)

    def append(self, sub: MetricScope, test: str | None, metric: str, num: float):
        row = [sub.case, sub.view, test, metric, sub.group, sub.file, num]
        self.rows.append(row)


def score_views(
    cases: list[Path], views: list[Path], testdirs: list[Path] | None
) -> DFBuilder:
    ret = DFBuilder()
    for view in views:
        for relp in get_inputs(cases, view):
            for case in cases:
                assert case.is_dir(), case
                if not (case / "input").exists():
                    continue
                scope = MetricScope(case.name, view.name, relp.parts[0], relp.name)
                inp = case / "input" / relp
                outp = case / "out1" / relp
                coverage = inp.exists() and outp.exists()
                ret.append(scope, "", "Cov", float(coverage))
                if coverage:
                    checks = [case / "check"] if testdirs is None else testdirs
                    inlines = file_lines_set(inp)
                    outlines = file_lines_set(outp)
                    weight = 1.0 - len(inlines & outlines) / len(inlines)
                    ret.append(scope, "", "W", weight)
                    for check in checks:
                        if check.exists():
                            m = score_metrics(inlines, outlines, check, relp)
                            ret.append(scope, check.name, "good", m.good)
                            ret.append(scope, check.name, "dbad", m.bad)
                            ret.append(scope, check.name, "dfail", m.fail)
                            ret.append(scope, check.name, "score", m.score)
    return ret


def main(cmd_line_args: Any = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument("cases", type=Path)
    parser.add_argument("view", type=Path, nargs="+")
    parser.add_argument("-m", "--metric", action='append')
    parser.add_argument("-t", "--test", action='append', type=Path)
    args = parser.parse_args(cmd_line_args)

    case_paths = get_case_paths(args.cases)
    scores = score_views(case_paths, args.view, args.test).to_data_frame()
    if args.metric:
        scores = scores.query(f"metric in {args.metric}")
    if len(case_paths) == 1:
        case = case_paths[0].name
        scores = scores.query(f"case == '{case}'")
        tab = scores.groupby(by=["group", "filename", "view", "test", "metric"]
                            ).mean(numeric_only=True)
    else:
        tab = scores.groupby(by=["case", "view", "test", "metric"]).mean(numeric_only=True)

    pd.options.display.float_format = '{:.0%}'.format
    print(tab.unstack(["view", "test", "metric"]).sort_index(axis=1))

    return 0


if __name__ == "__main__":
    exit(main())
