#!/usr/bin/python3

# Popular libraries
import jinja2, tomli

# Python Standard Library
import argparse, os
from pathlib import Path
from typing import Any


def capfirst(s: str) -> str:
    return s[0].upper() + s[1:] if s else s


def unilinify(s: str) -> str:
    """Make a TOML/template string expression that is
       a single-line, space-separated variant
       of the provided multi-line string.
    """
    return " \\\n".join(s.strip().split("\n"))


StructuredText = str | list["StructuredText"] | dict[str, "StructuredText"]

class Template:
    ENV = jinja2.Environment(
        trim_blocks=True,
        keep_trailing_newline=True,
        undefined=jinja2.StrictUndefined,
    )
    ENV.filters.update({'capfirst': capfirst, 'unilinify': unilinify})

    def __init__(self, src: StructuredText):
        self.tmpl: jinja2.Template | list[Template] | dict[str, Template]
        if isinstance(src, dict):
            self.tmpl = {k: Template(s) for k, s in src.items()}
        elif isinstance(src, list):
            self.tmpl = [Template(s) for s in src]
        else:
            self.tmpl = Template.ENV.from_string(src)

    def render(self, ctx: dict) -> StructuredText:
        if isinstance(self.tmpl, dict):
            return {k: t.render(ctx) for k, t in self.tmpl.items()}
        if isinstance(self.tmpl, list):
            return [t.render(ctx) for t in self.tmpl]
        return self.tmpl.render(**ctx)


SETTINGS_TOML_TMPL = Template('''\
chat_system = """\\
{{ system_prompt }}"""

max_tokens_ratio = 1.5

[openai]
model = '{{ openai_model }}'
temperature = 0
n = 1
''')


class ParamSpace:
    def __init__(self, spaces: list["ParamSpace"] = []):
        self._contexts: dict[tuple, dict[str, Any]]
        if not spaces:
            self._contexts =  {tuple(): dict()}
        else:
            self._contexts = dict()
            for sub in spaces:
                for pid, ctx in sub._contexts.items():
                    if pid in self._contexts:
                        pidstr = "-".join(pid)
                        raise SyntaxError(f"Two subspaces generate ID: {pidstr}")
                    self._contexts[pid] = ctx.copy()

    def copy(self):
        return ParamSpace([self])

    def items(self):
        return self._contexts.items()

    def render(self, vars: dict[str, Any]) -> None:
        for var_name, val in vars.items():
            tmpl = Template(val)
            for pid, ctx in self._contexts.items():
                ctx[var_name] = tmpl.render(ctx)

    def expand(self, params: dict[str, dict[str, Any]]) -> None:
        for param_name, args in params.items():
            self._expand1(param_name, args)

    def _expand1(self, param_name: str, args: dict[str, Any]) -> None:
        old_contexts = self._contexts
        self._contexts = dict()
        for key, arg in args.items():
            tmpl = Template(arg)
            for pid, ctx in old_contexts.items():
                newpid = (key,) + pid if key else pid
                self._contexts[newpid] = ctx | {param_name: tmpl.render(ctx)}


def get_expands_as_list(data):
    expands = data.get('expands', [])
    return expands if isinstance(expands, list) else [expands]


def new_space(space_data, existing: dict[str, ParamSpace]) -> ParamSpace:
    expands = get_expands_as_list(space_data)
    ret = ParamSpace([existing[name] for name in expands])
    ret.render(space_data.get('prevar', {}))
    ret.expand(space_data.get('param', {}))
    ret.render(space_data.get('postvar', {}))
    return ret


class SpaceLoader:
    def __init__(self, spec_file_data: dict[str, Any]):
        self.data = spec_file_data
        self.loaded: dict[str, ParamSpace] = dict()
        self._pending: set[str] = set()

    def load(self, space_name) -> ParamSpace:
        if space_name not in self.loaded:
            self._pending.add(space_name) # pending load, no dependency should cycle back
            space_data = self.data[space_name]
            for sub_name in get_expands_as_list(space_data):
                if sub_name in self.loaded:
                    if sub_name in self._pending:
                        msg = "Infinite reference loop made from {} to {}"
                        raise SyntaxError(msg.format(space_name, sub_name))
                else:
                    self.load(sub_name)
            self.loaded[space_name] = new_space(space_data, self.loaded)
            self._pending.remove(space_name)
        return self.loaded[space_name]


def validate_spec(spec_file_data: Any) -> None:
    if 'main' not in spec_file_data:
        raise SyntaxError("Missing main section")
    for space, data in spec_file_data.items():
        params = data.get('param', {})
        if not isinstance(params, dict):
            raise SyntaxError(f"'{space}.param' must be dictionary.")
        for param, args in params.items():
            if not args:
                raise SyntaxError(f"'{space}.param.{param}' missing values.")
            if not isinstance(args, dict):
                raise SyntaxError(f"'{space}.param.{param}' must be dictionary.")


def load_main_space(path: Path) -> ParamSpace:
    with open(path, 'rb') as file:
        spec_file_data = tomli.load(file)
    validate_spec(spec_file_data)
    return SpaceLoader(spec_file_data).load('main')


def main(cmd_line_args: Any = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument("spec", type=Path)
    parser.add_argument("dest", type=Path)
    parser.add_argument("-v", "--verbose", action='store_true')
    args = parser.parse_args(cmd_line_args)

    try:
        main = load_main_space(args.spec)
    except SyntaxError as ex:
        print("Error in", args.spec, ":", ex)
        return 2

    for pid, space_ctx in main.items():
        pidstr = "-".join(pid)
        system_prompt = space_ctx['system_prompt']
        if not system_prompt.endswith("\n"):
            system_prompt += "\\\n"
        print(pidstr)
        if args.verbose:
            print(system_prompt)
        txt = SETTINGS_TOML_TMPL.render({
            'system_prompt': system_prompt,
            'openai_model': 'gpt-4-0125-preview',
        })
        assert isinstance(txt, str)
        dest = args.dest / pidstr
        os.makedirs(dest, exist_ok=True)
        with open(dest / "settings.toml", 'w') as file:
            file.write(txt)

    return 0


if __name__ == "__main__":
    exit(main())
