#!/usr/bin/python3

import tomli

# Python Standard Library
import argparse, os, shutil, subprocess, tempfile
from pathlib import Path
from typing import Optional
from warnings import warn


COPYAID_TOML_TEMPLATE = r"""
openai_api_key_file = "~/.config/copyaid/openai_api_key.txt"
log_format = "jsoml"
tasks.it.request = "{}"
tasks.it.react = ["sent2line"]
commands.sent2line = '''sed -i 's/\. /.\n/g' "$@"'''
"""


def get_case_paths(cases: Path) -> list[Path]:
    if cases.is_dir():
        case_contents = {"settings.toml", "input"}
        if all((cases / sub).exists() for sub in case_contents):
            ret = [cases]
        else:
            ret = [p for p in cases.iterdir() if p.is_dir()]
    else:
        with open(cases) as cases_file:
            ret = list()
            for line in cases_file:
                line = line.strip()
                found = list(cases.parent.glob(line))
                if not found:
                    warn(f"NOT FOUND: {line}")
                ret.extend(found)
    return ret


def write_config(settings_path: Path, tmp_dir: Path) -> None:
    with open(settings_path, "rb") as file:
        settings = tomli.load(file)
    assert settings["openai"].get("n", 1) == 1
    assert settings["openai"]["temperature"] == 0
    with open(tmp_dir / "copyaid.toml", "w") as file:
        file.write(COPYAID_TOML_TEMPLATE.format(settings_path.resolve()))


class Blaster:
    def __init__(self, case: Path, tmp_dir: Path, dry: bool):
        self.case = case
        self.tmp_dir = Path(tmp_dir)
        self.dry = dry
        assert self.tmp_dir.is_dir()
        if not self.dry:
            write_config(case / "settings.toml", tmp_dir)

    def _copyaidit(self, src: Path) -> Path:
        tmp = str(self.tmp_dir)
        cmdline = ["copyaid", "it", "-c", tmp, "-d", tmp, str(src)]
        print(cmdline)
        subprocess.run(cmdline, check=True)
        return self.tmp_dir / "R1" / src.name

    def get_first_output(self, group: str, filename: str) -> Path:
        src = self.case / "input" / group / filename
        out = self.case / "out1" / group / filename
        if not out.exists():
            print(out)
            if not self.dry:
                result = self._copyaidit(src)
                os.makedirs(out.parent, exist_ok=True)
                shutil.copy(result, out)
        return out


def main(cmd_line_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("cases", type=Path)
    parser.add_argument("--dry", action='store_true')
    args = parser.parse_args(cmd_line_args)

    for case in get_case_paths(args.cases):
        inputs = case / "input"
        if case.is_dir() and inputs.exists():
            print(case.name)
            with tempfile.TemporaryDirectory() as tmp_dir:
                blaster = Blaster(case, Path(tmp_dir), args.dry)
                for group in os.listdir(inputs):
                    for filename in os.listdir(case / "input" / group):
                        blaster.get_first_output(group, filename)


if __name__ == "__main__":
    exit(main())
