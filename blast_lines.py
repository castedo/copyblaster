#!/usr/bin/python3

from blast_cases import get_case_paths, Blaster
from score_outputs import get_inputs

# Python Standard Library
import argparse, hashlib, os, shutil, tempfile
from dataclasses import dataclass
from pathlib import Path
from warnings import warn
from typing import Tuple


GROUP = "lines"
MIN_TOKENS = 6
MIN_COMMON_TOKENS = 4


class File:
    def __init__(self, path: Path):    
        assert path.exists()
        with open(path, 'rb') as file:
            content = file.read()
        self.path = path
        self.hash = hashlib.md5(content).hexdigest()
        self.num_lines = content.count(b"\n")
        self.tokens = set(t.lower() for t in content.split())

    def ensure_copy(self, other: Path) -> "File":
        if not other.exists():
            shutil.copyfile(self.path, other)
        ret = File(other)
        assert self.hash == ret.hash
        return ret

    def looks_like_rev_of(self, orig: "File") -> bool:
        if len(self.tokens) < MIN_TOKENS:
            warn(f"{self.path} has very few words.")
            return False
        if len(self.tokens & orig.tokens) < MIN_COMMON_TOKENS:
            warn(f"{self.path} kept very few words.")
            return False
        return True


class LineBlaster(Blaster):

    def get_output(self, file: File) -> File | None:
        assert file.hash == file.path.stem
        group = file.path.parent.name
        out_path = self.get_first_output(group, file.path.name)
        if not out_path.exists():
            print("Non-stats run needed.")
            return None
        out = File(out_path)
        return out if out.looks_like_rev_of(file) else None

    def blast_line(self, start: Path, max_num_steps: int) -> Tuple[int, int | None]:
        assert max_num_steps < 16
        start_file = File(start)
        file = start_file
        revised = []
        num_revs = 0
        while (file.hash not in revised) and (out := self.get_output(file)):
            revised.append(file.hash)
            if not out.looks_like_rev_of(start_file):
                break
            num_revs += 1
            next_input_path = file.path.parent / (out.hash + file.path.suffix)
            if not next_input_path.exists() and num_revs >= max_num_steps:
                warn(f"{out.path} reached max num revisions {max_num_steps}")
                break
            file = out.ensure_copy(next_input_path)
        loop_size = None
        if out and out.hash in revised:
            loop_size = len(revised) - revised.index(out.hash) - 1
        if max_num_steps > 0:
            print(" ".join(s[:5] for s in revised), loop_size)
        return (num_revs - 1, loop_size)


@dataclass
class ChainTally:
    num_chains: int = 0
    num_loopers: int = 0
    tot_mods : int = 0

    def add(self, num_mods, loop_size):
        self.num_chains += 1
        self.tot_mods += num_mods
        if loop_size: 
            self.num_loopers += 1

    def frac_loopers(self):
        return self.num_loopers / self.num_chains

    def ave_mods(self):
        return self.tot_mods / self.num_chains


def main(cmd_line_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("cases", type=Path)
    parser.add_argument("view", type=Path)
    parser.add_argument("--run", type=int, default=0)
    args = parser.parse_args(cmd_line_args)

    for case in get_case_paths(args.cases):
        lines = case / "input" / GROUP
        if case.is_dir() and lines.exists():
            tally = ChainTally()
            with tempfile.TemporaryDirectory() as tmp_dir:
                blaster = LineBlaster(case, Path(tmp_dir), not args.run)
                for relp in get_inputs([case], args.view):
                    assert relp.parts[-2] == GROUP
                    nums = blaster.blast_line(case / "input" / relp, args.run)
                    tally.add(*nums)
            print(case.name, tally.frac_loopers(), tally.ave_mods())


if __name__ == "__main__":
    exit(main())
